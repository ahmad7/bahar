public class Weather {


	private float temprature;
	private float humidity;
	
	public Weather() {
		// TODO Auto-generated constructor stub
		this.humidity = 0;
		this.temprature = 0;
	}
	public Weather(float temp,float humid){
		this.temprature = temp;
		this.humidity = humid;
	}
	
	public void setWeather(Weather a){
		this.temprature = a.getTemprature();
		this.humidity = a.getHumidity();
	}
	
	public float getTemprature() {
		return temprature;
	}
		
	public float getHumidity() {
		return humidity;
	}

	public void setTemprature(float temp) {
		temprature = temp;
	}

	public void setHumidity(float hum) {
		humidity = hum;
	}
	
	public String toString(){
		return Float.toString(temprature)+" "+Float.toString(humidity);
	}
}
