/*
 * This class represent a Hive object
 * it contains information about hive, 
 * like weather, temperature, number of 
 * floor and etc.
 */
public class Hive {
	public Weather hiveWeather;
	public int numberOfFloors;
	
	Hive(){
		this.hiveWeather = new Weather();
		this.numberOfFloors = 1;
	}
	Hive(Weather a,int num){
		this.hiveWeather = new Weather();
		this.hiveWeather.setWeather(a);
		this.numberOfFloors = num;
	}
	
}
