import com.db4o.*;
import java.util.Scanner;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;

public class Main extends Util {

	private static Image img;
	static ObjectContainer db;
	
	final static String DB4OFILENAME = System.getProperty("user.home")
			+ "/bees2.db4o";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
		db = Db4oEmbedded.openFile(config, DB4OFILENAME);
		try {
			// do something with db4o
			img = new Image();
			showMenu();
		} 
		finally{
			db.close();
		}
	}
	public static void showMenu() {
		Scanner scanIn = new Scanner(System.in);
		while (true) {
			System.out.print("1. Insert.\n2. Update.\n3. Delete.\n4. Close.Please select an item: ");
			int choice = scanIn.nextInt();
			switch (choice){
			case 1:
				insertIntoDB();
				break;
			case 3:
				deleteFromDB();
				break;
			case 2:
				updateDB();
				break;
			case 4:
				db.close();
				return;
			default:
				System.out.println("Please select 1, 2, 3 or 4...");
			}
		}
	}
	public static Boolean insertIntoDB() {
		img.readInput();
		db.store(img);
		Weather a = new Weather();
		db.store(a);
		return true;
	}
	public static void listImages()
	{
		ObjectSet result = db.queryByExample(Image.class);
		for (Object o : result) {
			((Image)o).printImg();
		}
	}
	public static Boolean deleteFromDB() {
		/*db.delete((Object)new Weather(23,23));
		System.out.println(db.equals((Object)new Weather(23,23)));*/
		ObjectSet result = db.queryByExample(new Weather(23,23));
		Weather found = (Weather) result.next();
		db.delete(found);
		//listImages();
		return true;
	}
	public static Boolean updateDB() {
		ObjectSet<Weather> result = db.query(Weather.class);
		for(Weather row : result){
			if(row.getHumidity()==23)
				System.out.println(row);
		}
		return true;
	}
}
