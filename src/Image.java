import java.util.Date;
import java.util.Scanner;
import java.text.*;

public class Image {


	private Weather weather;
	private Date date;
	private String URL;
	private int estimatedBeesNumber;
	private double entropy;
	private double SNR;
	
	public Image() {
		weather = new Weather();
	}

	public void readInput() {
		System.out.print("Please Enter Date and Time(yyyy.MM.dd HH:mm:ss): ");
		Scanner scanIn = new Scanner(System.in);
		String str = scanIn.nextLine();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		try {
			date = ft.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print("Please Enter URL for the image: ");
		URL = scanIn.nextLine();
		System.out
				.print("Please Enter the estimated number of bees in the image: ");
		estimatedBeesNumber = scanIn.nextInt();
		System.out
				.print("Please Enter the entropy calculated from the image: ");
		entropy = scanIn.nextDouble();
		System.out.print("Please Enter SNR of the image: ");
		SNR = scanIn.nextDouble();
		System.out.print("Please Enter temprature: ");
		weather.setTemprature(scanIn.nextFloat());
		System.out.print("Please Enter humidity: ");
		weather.setHumidity(scanIn.nextFloat());
	}

	public void printImg() {
		
		System.out.print("\n\n");
				
		System.out.println("Date and Time: "
				+ date.toString());

		System.out.println("URL for the image: " + URL);

		System.out.println("Estimated number of bees in the image: "
				+ estimatedBeesNumber);

		System.out.println("Entropy calculated from the image: " + entropy);

		System.out.println("SNR of the image: " + SNR);

		System.out.println("Temprature: " + weather.getTemprature());

		System.out.println("Humidity: " + weather.getHumidity());
		
		System.out.println("------------------------------------------------------");
	}
}
